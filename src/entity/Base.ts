/**
 * 基础字段
 */
export default interface Base {

    /**
     * ID
     */
    id?: number;

}