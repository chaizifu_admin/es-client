enum LocalStorageKeyEnum {

    /**
     * 设置
     */
    SETTING = 'setting',

    /**
     * 编辑器设置
     */
    EDITOR_SETTING = 'editor-setting',

    /**
     * es-client当前的版本
     */
    VERSION = 'es-client.version',

    /**
     * 最后使用的链接
     */
    LAST_URL = 'last-url'

}

export default LocalStorageKeyEnum;