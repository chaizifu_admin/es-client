enum TableNameEnum {

    /**
     * 链接
     */
    URL = 'url',

    /**
     * 基础搜索历史
     */
    BASE_SEARCH_HISTORY = 'baseSearchHistory',

    /**
     * 高级搜索历史
     */
    SENIOR_SEARCH_HISTORY = 'seniorSearchHistory'

}

export default TableNameEnum;