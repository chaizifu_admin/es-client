enum DownloadTypeEnum {

    JSON = 'json',

    YML = 'yml',

    XML = 'xml',

    HTML = 'html',

    CSV = 'csv',

    TXT = 'txt'

}

export default DownloadTypeEnum;