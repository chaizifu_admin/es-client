enum HttpTypeEnum {

    /**
     * 浏览器
     */
    BROWSER = 'browser',

    /**
     * utools
     */
    UTOOLS = 'utools',

    /**
     * 桌面客户端
     */
    DESKTOP = 'desktop',

    /**
     * 服务器模式
     */
    SERVER = 'server',

    /**
     * web模式
     */
    WEB = 'web'

}

export default HttpTypeEnum;