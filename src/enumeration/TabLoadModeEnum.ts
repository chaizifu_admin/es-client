/**
 * 标签载入模式
 */
enum TabLoadModeEnum {

    /**
     * 追加
     */
    APPEND= 1,

    /**
     * 覆盖
     */
    COVER = 2

}

export default TabLoadModeEnum;