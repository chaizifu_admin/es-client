enum PageNameEnum {

    HOME = "home",

    DATA_BROWSER = "data-browser",

    BASE_SEARCH = "base-search",

    SENIOR_SEARCH = "senior-search",

    SETTING = "setting"

}

export default PageNameEnum;